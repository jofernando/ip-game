from controller.ponto_turistico import *
from controller.perstistencia import *
import arcade
from model.constantes import *


class ControllerJanela:
    def __init__(self):
        self.score = 0
        self.state = 'character'
        self.controller_ponto_turistico = ControllerPontoTuristico()
        self.lugar_atual = None

    def setup(self):
        if verificar_se_arquivo_existe('score.pck') and verificar_se_arquivo_possui_conteudo('score.pck'):
            self.load()
        self.definir_funcoes_para_botoes_dos_pontos_turisticos()

    def definir_funcoes_para_botoes_dos_pontos_turisticos(self):
        for key in self.controller_ponto_turistico.pontos_turisticos:
            for j in self.controller_ponto_turistico.pontos_turisticos[key].pergunta.alternativas:
                if j.eh_correta:
                    j.definir_acao(self.aumentar_pontuacao)
                else:
                    j.definir_acao(self.diminuir_pontuacao)

    def save(self):
        persistir_objeto(self.score, 'score.pck')

    def load(self):
        self.score = recuperar_objeto_persistido('score.pck')

    def diminuir_pontuacao(self):
        if self.score > 0:
            self.score -= 1

    def aumentar_pontuacao(self):
        self.score += 3
        self.state = 'map'
        self.save()

    @staticmethod
    def verificar_personagem_selecionado(x, y, skin):
        for i in skin:
            if x < i.left:
                continue
            elif x > i.right:
                continue
            elif y > i.top:
                continue
            elif y < i.bottom:
                continue
            return i.name

    def check_mouse_press_for_buttons(self, x, y, button_list):
        """ Given an x, y, see if we need to register any button clicks. """
        for button in button_list:
            # print(button, 'checkmousepress')
            # print(f'x {x} y {y}')
            # print(f'left {button.center_x - button.width / 2} right {button.center_x + button.width / 2} top {button.center_y + button.height / 2} bot {button.center_y - button.height / 2}')
            if x > button.center_x + button.width / 2:
                continue
            if x < button.center_x - button.width / 2:
                continue
            if y > button.center_y + button.height / 2:
                continue
            if y < button.center_y - button.height / 2:
                continue
            # print('chegou')
            button.on_press()

    def check_mouse_release_for_buttons(self, x, y, button_list):
        """ If a mouse button has been released, see if we need to process
            any release events. """
        for button in button_list:
            # print(button,'checkmouserelease')
            if button.pressed:
                # print('chamou')
                button.on_release()

    def verificar_skin_selecionada(self, x, y, skins):
        if self.state == 'character':
            name = self.verificar_personagem_selecionado(x, y, skins)
            if name is not None:
                self.state = 'map'
                return name

    def verificar_button_selecionado(self, x, y):
        if self.state == 'tourist_spot':
            tourist_spot = self.controller_ponto_turistico.pontos_turisticos[self.lugar_atual]
            button_list = []
            for i in tourist_spot.pergunta.alternativas:
                # print(i.button,'onmousepress')
                button_list.append(i.button)
            self.check_mouse_press_for_buttons(x, y, button_list)

    def movimentar_personagem(self, symbol, personagem):
        if self.state == 'map':
            if symbol == arcade.key.UP:
                personagem.change_y = MOVEMENT_SPEED
            elif symbol == arcade.key.DOWN:
                personagem.change_y = -MOVEMENT_SPEED
            elif symbol == arcade.key.LEFT:
                personagem.change_x = -MOVEMENT_SPEED
            elif symbol == arcade.key.RIGHT:
                personagem.change_x = MOVEMENT_SPEED

    def parar_de_movimentar_personagem(self, symbol, personagem):
        if self.state == 'map':
            if symbol == arcade.key.UP or symbol == arcade.key.DOWN:
                personagem.change_y = 0
            elif symbol == arcade.key.LEFT or symbol == arcade.key.RIGHT:
                personagem.change_x = 0
