from model.pergunta import Pergunta
from model.alternativa import Alternativa
from model.ponto_turistico import PontoTuristico


# coding:utf-8

class ControllerPontoTuristico:
    def __init__(self):
        self.pontos_turisticos = self.definir_pontos_turisticos()

    def definir_pontos_turisticos(self):
        pontos_turisticos = {}
        historia = 'Mãe peregrina.\nMãe Peregrina é o centro de espiritualidade do Movimento Apostólico ' \
                   'Internacional de Schoenstatt, que é um movimento católico mariano fundado em Schoenstatt, ' \
                   'na Alemanha, em 1914, pelo Padre José Kentenich. Neste momento existem cerca de 200 ' \
                   'santuários espalhados pelo mundo. Nas regiões de idioma português, 23 santuários são no ' \
                   'Brasil a cidade  de Garanhuns, foi agraciada com a escolha para abrigar o Santuário da ' \
                   'Mãe Rainha de Schoenstatt, o que foi considerado pelo seu povo como uma dádiva divina. ' \
                   'A construção do Santuário em Garanhuns teve início em março de 2002. Desde que se iniciou ' \
                   'a construção, muitas pessoas peregrinam diariamente a este lugar. Sua inauguração solene ' \
                   'aconteceu em 18 de abril de 2004, numa grande manifestação de fé e alegria, com a' \
                   ' presença de 40 mil pessoas vindas de todos os estados do Brasil. Hoje o Santuário ' \
                   'de Mãe Rainha tornou-se um lugar muito querido para o povo que peregrina a este Trono ' \
                   'de graças para buscar paz e encontrar-se com Deus.'
        text = "Qual o movimento que trouxe O Santuário da Mãe Rainha para Garanhuns?"
        alternativa1 = Alternativa("Movimento Apostólico Internacional de Schoenstatt", True)
        alternativa2 = Alternativa("Movimento Mariano de Paris")
        alternativa3 = Alternativa("O Prefeito")
        alternativas = [alternativa1, alternativa2, alternativa3]
        pergunta = Pergunta(text, alternativas)
        ponto_turistico = PontoTuristico(historia, pergunta)
        pontos_turisticos['ponto01'] = ponto_turistico
        historia = 'Centro Cultural Alfredo Leite Cavalcanti\nPernambuco foi o segundo estado brasileiro a ' \
                   'possuir malha ferroviária e o primeiro do Nordeste. Inaugurada em 28.09.1887, faz parte do ' \
                   'ramal Garanhuns, um prolongamento da Estrada de Ferro Sul de Pernambuco que faz a ligação ' \
                   'entre Recife e Alagoas. A estação funcionou até a década de 1960 e, em 1979, foi restaurada ' \
                   'e transformada, a gare, parte central com cobertura que liga a Estação e o Armazém, foi ' \
                   'fechada e hoje funciona o Teatro Luiz Souto Dourado. O prédio preserva a arquitetura inglesa ' \
                   'do século XIX, está localizada no centro da cidade e funciona como Centro Cultural Alfredo ' \
                   'Leite, abrigando exposições e atividades culturais.'
        text = "Qual a forma dos aliens que trouxeram o relogio?"
        alternativa1 = Alternativa("Um armazém")
        alternativa2 = Alternativa("Um clube")
        alternativa3 = Alternativa("Uma estação de trem", True)
        alternativas = [alternativa1, alternativa2, alternativa3]
        pergunta = Pergunta(text, alternativas)
        ponto_turistico = PontoTuristico(historia, pergunta)
        pontos_turisticos['ponto02'] = ponto_turistico

        historia = 'Cristo do Magano\nA altura média de 1.030 metros em relação a ao nível do mar,no extremo Oeste, ' \
                   'o Alto do Magano é um dos mais belos pontos turísticos da cidade de Garanhuns, com a construção ' \
                   'do Mirante do Cristo Redentor em 1957, podemos admirar a mais bela paisagem natural e cultural ' \
                   'do Planalto da Borburema, avistando a colinas da cidade nos pontos extremos bem distintos. Ao ' \
                   'norte: Quilombo; ao Sul Ipiranga; a Leste Monte Sinai e Triunfo; Sudeste Columinho e Sudoeste ' \
                   'Antas.'
        text = "Qual a altura do Alto do Magano?"
        alternativa1 = Alternativa("200 metros")
        alternativa2 = Alternativa("500 metros")
        alternativa3 = Alternativa("1030 metros", True)
        alternativas = [alternativa1, alternativa2, alternativa3]
        pergunta = Pergunta(text, alternativas)
        ponto_turistico = PontoTuristico(historia, pergunta)
        pontos_turisticos['ponto03'] = ponto_turistico

        historia = 'Relógio Das Flores\nConstruído em 1979, o Relógio das Flores é formado, exclusivamente, por ' \
                   'plantas e flores, desde seus números aos ponteiros. Funciona a cristal de quartzo e atrasa ' \
                   'apenas um minuto por ano. É o único no Norte e Nordeste nesse estilo, medindo 4 metros de ' \
                   'diâmetro. A praça Tavares Correia também merece destaque. Rodeado por árvores frondosas e ' \
                   'flamboyans, o lugar é bastante aprazível durante o ano inteiro, ainda mais durante a primavera, ' \
                   'quando as flores se multiplicam, cheias de cores e aromas, atraindo centenas de borboletas.' \
                   'Além de muitos turistas.'
        text = "Quantos minutos o Relógio de Flores atrasa por ano?"
        alternativa1 = Alternativa("1", True)
        alternativa2 = Alternativa("5")
        alternativa3 = Alternativa("12")
        alternativas = [alternativa1, alternativa2, alternativa3]
        pergunta = Pergunta(text, alternativas)
        ponto_turistico = PontoTuristico(historia, pergunta)
        pontos_turisticos['ponto04'] = ponto_turistico

        historia = 'Parque Ruber van der Linden Nascido em 1896, Ruber acabou se tornando engenheiro eletricista. ' \
                   'Herdou do pai o amor pela literatura, escrevia seus livros e participava de jornais que ' \
                   'circulavam na cidade no início do século passado.A sua grande obra foi no exercício da função ' \
                   'principal. Ele foi responsável por elaborar o plano de abastecimento de água e luz da cidade.' \
                   'No mesmo lugar onde ficava a sede da Companhia, o engenheiro começou a idealizar um parque. ' \
                   'Na época, praticamente não se falava em temas ecológicos, preservação ambiental, esse tipo de' \
                   ' coisa.A área serviria, segundo Linden, pra descanso dos trabalhadores da Companhia de ' \
                   'Eletricidade e passeio das famílias após uma cansativa semana de trabalho.Porém, um outro ' \
                   'objetivo do parque, ainda que não fosse o principal, era criar uma cobertura vegetal pra ' \
                   'proteger as nascentes de rios que saíam dali. O parque foi, a bem da verdade, a primeira ' \
                   'área com características verdadeiramente ecológicas no interior do estado de Pernambuco.' \
                   'Após a morte do seu criador, em 1949, a prefeitura municipal resolveu que o parque se ' \
                   'chamaria oficialmente “Ruber van der Linden”, mas não adianta chegar lá em Garanhuns e ' \
                   'perguntar a qualquer pessoa por esse nome. “Pau Pombo” é como o local é conhecido e assim ficou.'
        text = "Qual o primeiro motivo da criação do parque?"
        alternativa1 = Alternativa("Para criar uma área verde perto das nascentes de rios")
        alternativa2 = Alternativa("Para servir de distração para os trabalhadores e suas famílias", True)
        alternativa3 = Alternativa("Para ser a primeira reserva do interior de Pernambuco")
        alternativas = [alternativa1, alternativa2, alternativa3]
        pergunta = Pergunta(text, alternativas)
        ponto_turistico = PontoTuristico(historia, pergunta)
        pontos_turisticos['ponto05'] = ponto_turistico

        historia = 'Castelo de João Capão\nJoão desenhou o castelo ainda aos oito anos, a sua construção já se estende'\
                   'por mais 30 anos, e continua durante as suas folgas e com o recursos próprios, O castelo tem ' \
                   'torres pátio, fonte, tudo o que um castela medieval tem, joão contava que um dia ao olhar para ' \
                   'sua casa humilde, ainda na infância disse para mãe que construiria um reinado para eles ' \
                   'morarem, e aos trinta e quatro anos de idade ele começou, conta que sua mãe chegou a ver o ' \
                   'castelo que ele prometeu, e ele trabalhou sempre com recursos próprios, para realizar seu sonho, ' \
                   'hoje é um dos pontos turísticos da cidade de Garanhuns, e a estrutura é igual a que ele desenhou ' \
                   'para mãe quando tinha 8 anos de idade.'
        text = "Com quantos anos de idade João desenhou o castelo?"
        alternativa1 = Alternativa("15 anos")
        alternativa2 = Alternativa("12 anos")
        alternativa3 = Alternativa("8 anos", True)
        alternativas = [alternativa1, alternativa2, alternativa3]
        pergunta = Pergunta(text, alternativas)
        ponto_turistico = PontoTuristico(historia, pergunta)
        pontos_turisticos['ponto06'] = ponto_turistico

        return pontos_turisticos
