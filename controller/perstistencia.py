import pickle


def recuperar_objeto_persistido(nome_arquivo):
    if not verificar_se_arquivo_existe(nome_arquivo):
        return None
    if not verificar_se_arquivo_possui_conteudo(nome_arquivo):
        return None
    arq = open(nome_arquivo, 'rb')
    objeto = pickle.load(arq)
    arq.close()
    return objeto


def persistir_objeto(objeto, nome_arquivo):
    arq = open(nome_arquivo, 'wb')
    pickle.dump(objeto, arq)
    arq.close()


def verificar_se_arquivo_possui_conteudo(nome_arquivo):
    try:
        arq = open(nome_arquivo, 'rb')
        if arq.read() != '':
            return True
        else:
            return False
    except IOError:
        raise IOError()


def verificar_se_arquivo_existe(nome_arquivo):
    try:
        arq = open(nome_arquivo)
        arq.close()
        return True
    except IOError:
        return False
