import arcade
from model.constantes import *
from controller.janela import ControllerJanela
from view.skin import Skin
from view.lugar import Lugar
from view.personagem import Personagem


class MinhaJanela(arcade.Window):
    def __init__(self, width: float = 800, height: float = 600, title: str = 'Arcade Window', fullscreen: bool = False,
                 resizable: bool = False):
        super().__init__(width, height, title, fullscreen, resizable)
        self.controller_janela = ControllerJanela()
        self.background_map = None
        self.background_character = None
        self.skins = None
        self.lugares = None
        self.personagem = None

    def setup(self):
        self.controller_janela.setup()
        self.background_map = arcade.load_texture('../mapa01.png')
        self.background_character = arcade.load_texture('../background_character.png')
        self.skins = arcade.SpriteList()
        self.definir_lugares()

    def on_draw(self):
        arcade.start_render()
        if self.controller_janela.state == 'character':
            arcade.draw_texture_rectangle(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, SCREEN_WIDTH, SCREEN_HEIGHT,
                                          self.background_character)
            self.desenhar_skins()
            self.skins.draw()
        if self.controller_janela.state == 'map':
            arcade.draw_texture_rectangle(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, SCREEN_WIDTH, SCREEN_HEIGHT,
                                          self.background_map)
            self.personagem.draw()
            self.lugares.draw()
            arcade.draw_text(f'Score {self.controller_janela.score}', 50, 50, arcade.color.WHITE)
        if self.controller_janela.state == 'tourist_spot':
            self.controller_janela.controller_ponto_turistico.pontos_turisticos[self.controller_janela.lugar_atual].draw()

    def on_update(self, delta_time: float):
        # print(f'{self.controller_janela.state}')
        if self.controller_janela.state == 'map':
            self.lugares.update()
            self.personagem.update()
            hit_lugar = arcade.check_for_collision_with_list(self.personagem, self.lugares)
            if len(hit_lugar) > 0:
                current_spot = hit_lugar[0].ponto_turistico
                self.ir_para_ponto_turistico()
                # print(current_spot)
                self.controller_janela.lugar_atual = current_spot

    def on_mouse_press(self, x: float, y: float, button: int, modifiers: int):
        name = self.controller_janela.verificar_skin_selecionada(x, y, self.skins)
        if name is not None:
            self.definir_personagem(name)
        self.controller_janela.verificar_button_selecionado(x, y)

    def on_key_press(self, symbol: int, modifiers: int):
        self.controller_janela.movimentar_personagem(symbol, self.personagem)

    def on_key_release(self, symbol: int, modifiers: int):
        self.controller_janela.parar_de_movimentar_personagem(symbol, self.personagem)

    def ir_para_ponto_turistico(self):
        self.controller_janela.state = 'tourist_spot'
        self.personagem.center_x = SCREEN_WIDTH / 2
        self.personagem.center_y = SCREEN_HEIGHT / 2
        self.personagem.change_x = 0
        self.personagem.change_y = 0
        # print(ponto_turistico)

    def definir_lugares(self):
        self.lugares = arcade.SpriteList()
        lugar = Lugar('../ponto_01.png', 'ponto01')
        lugar.center_x = 78
        lugar.center_y = 283
        self.lugares.append(lugar)
        lugar = Lugar('../ponto_02.png', 'ponto02')
        lugar.center_x = 314
        lugar.center_y = 370
        self.lugares.append(lugar)
        lugar = Lugar('../ponto_03.png', 'ponto03')
        lugar.center_x = 560
        lugar.center_y = 340
        self.lugares.append(lugar)
        lugar = Lugar('../ponto_04.png', 'ponto04')
        lugar.center_x = 734
        lugar.center_y = 86
        self.lugares.append(lugar)
        lugar = Lugar('../ponto_05.png', 'ponto05')
        lugar.center_x = 490
        lugar.center_y = 35
        self.lugares.append(lugar)
        lugar = Lugar('../ponto_06.png', 'ponto06')
        lugar.center_x = 270
        lugar.center_y = 68
        lugar.scale = 0.1
        self.lugares.append(lugar)

    def definir_personagem(self, name):
        self.personagem = Personagem('../' + name + '.png', center_x=400, center_y=200)
        self.personagem.scale = 0.1

    def desenhar_skins(self):
        x = 0
        y = SCREEN_HEIGHT / 2
        for i in ('indio', 'india', 'guara'):
            character = Skin('../' + i + ".jpg", i)
            x += 100
            character.center_y = y
            character.center_x = x
            self.skins.append(character)

    def verificar_personagem_selecionado(self, x, y, personagens):
        return self.controller_janela.verificar_personagem_selecionado(x, y, personagens)


def main():
    janela = MinhaJanela(800, 400)
    janela.setup()
    arcade.run()

main()