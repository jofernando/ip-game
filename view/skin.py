import arcade

class Skin(arcade.Sprite):
    def __init__(self, filename, name):
        super(Skin, self).__init__(filename)
        self.name = name