from model.button import Button


class Alternativa:
    def __init__(self, text, eh_correta=False):
        self.text = text
        self.action_function = None
        self.eh_correta = eh_correta
        self.button = None

    def definir_acao(self, action_function):
        self.action_function = action_function

    def draw(self, center_x, center_y, width, height):
        self.button = Button(center_x, center_y, width, height, self.text, self.action_function)
        self.button.draw()
