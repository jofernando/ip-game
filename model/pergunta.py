from model.constantes import *
import arcade


class Pergunta:
    def __init__(self, text, alternativas):
        self.text = text
        self.alternativas = alternativas

    def draw(self, start_x, start_y):
        arcade.draw_text(self.text, start_x, start_y, arcade.color.WHITE, font_size=14, width=SCREEN_WIDTH - 40)
        for index, value in enumerate(self.alternativas):
            value.draw(SCREEN_WIDTH / 2, 20 + (index * 35), SCREEN_WIDTH - 30, 30)
