from model.constantes import *
import arcade


class PontoTuristico:
    def __init__(self, historia, pergunta):
        self.historia = historia
        self.pergunta = pergunta

    def draw(self):
        center_x = SCREEN_WIDTH / 2
        center_y = SCREEN_HEIGHT / 2
        arcade.draw_rectangle_filled(center_x, center_y, SCREEN_WIDTH, SCREEN_HEIGHT, arcade.color.JADE)
        arcade.draw_text(self.historia, center_x - SCREEN_WIDTH / 2 + 20, center_y + SCREEN_HEIGHT / 2 - 20,
                         arcade.color.WHITE, width=SCREEN_WIDTH - 40, font_size=11)
        self.pergunta.draw(20, 120)
