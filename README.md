Cronograma do projeto de IP 


O projeto parte do princípio de criação de um programa para o ensino da história de garanhuns na própria cidade, contando um pouco sobre os pontos turísticos e 

sua história, de a criança ou usuário poderá escolher entre 4 personagens ligados a história de Garanhuns(um índio, uma índia, um lobo guará, e um Anu, para 

percorrer os pontos da cidade descritos, lêr um texto sobre ele, e pontuar ao responder perguntas, ao acertar ele acumula pontos e aciona o save point 

automaticamente, ao errar ele tem outra chance, porém a pontuação recebida será menor, foi dividido o programa de acordo com o Sistema MVC (Model, View, Controller)

orientado a objetos.
	
	
![Diagrama de classes](https://gitlab.com/jofernando/ip-game/raw/master/doc/diagrama_classes.png)


![Diagrama mvc](https://gitlab.com/jofernando/ip-game/raw/master/doc/cronograma-mvc.png)